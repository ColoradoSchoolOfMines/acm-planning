Contact info: *primary contact for organizing this talk*

- [ ] Contact company
- [ ] Confirm Speaker ()
- [ ] Confirm Topic ()
- [ ] Confirm Date ()
- [ ] Confirm Title ()
- [ ] Update calendar on website
- [ ] @sumner create flyer
- [ ] @robozman Email the officers, Kelly, Tracy, CPW, and Shannon to get the
      ball rolling on advertising the talk
- [ ] Submit to Daily Blast
- [ ] A day before the talk, send a short email to remind the presenter and
      offer to answer any last minute questions
- [ ] A day after the talk, send a short thank you email to the presenter.
