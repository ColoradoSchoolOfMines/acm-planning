<!-- Use "Plan <date> Meeting" as the issue title -->

/due <date>

# TODO

- [ ] @jhgarner set up a survey
- [ ] @warfields make sure food arrives at the meeting
- [ ] Make sure we have enough plates
- [ ] Make sure we have enough cups

# Things to Mention at the beginning of the meeting

- [put announcements here]

# Meeting Plan

- [do this]
- [then that]

/label tracking
